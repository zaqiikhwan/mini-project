

# C# Starter Pack

### Table of Content ###
- [Overview](#overview)
- [1. Getting Started](#1-getting-started)
    - [1.1. Code Setup](#11-code-setup)
    - [1.2. Database Setup](#12-database-setup)
    - [1.3. Sample Project](#13-sample-project)
    - [1.4. Bare Project](#14-bare-project)
- [2. Project Structure](#2-project-structure)
    - [2.1. Data Access](#21-data-access)
    - [2.2. Integration](#22-integration)
    - [2.3. Utilities](#22-integration)
    - [2.4. Web App](#24-web-app)
- [3. Plugins](#3-plugins)
    - [3.1. Entity Framework](#31-entity-framework)
    - [3.2. Npgsql](#32-npgsql)
    - [3.3. Hangfire](#33-hangfire)
    - [3.4. Ninject](#34-ninject)
    - [3.5. OpenXML](#35-openxml)
    - [3.6. AutoMapper](#36-automapper)
    - [3.7. Elmah](#37-elmah)
    - [3.8. Swagger](#38-swagger)
- [4. Development](#4-development)
    - [4.1. Menambahkan dan Mengubah Model](#41-menambahkan-dan-mengubah-model)
    - [4.2. Menambahkan dan Mengubah Service](#42-menambahkan-dan-mengubah-service)
    - [4.3. Menambahkan dan Mengubah Controller](#43-menambahkan-dan-mengubah-controller)
    - [4.4. Menambahkan dan Mengubah View](#44-menambahkan-dan-mengubah-view)
    - [4.5. Menambahkan Area](#45-menambahakan-areas)
- [5. Available Services](#5-available-services)
    - [5.1. IEmailServices](#51-iemailservices)
    - [5.2. IExcelFileServices](#52-iexcelfileservices)
    - [5.3. IFileManServices](#53-ifilemanservices)
    - [5.4. IMigrasiDataServices](#54-imigrasidataservices)
    - [5.5. IWordTextReplacementServices](#55-iwordtextreplacementservices)
    - [5.6. IWorkflowServices](#56-iworkflowservices)
- [6. Helper](#6-helper)
    - [6.1. Static Method Helper](#61-static-method-helper)
    - [6.2. Extension Method Helper](#62-extension-method-helper)
    - [6.3. Attribute Helper](#63-attribute-helper)
    - [6.4. Datatable Helper](#64-datatable-helper)
    - [6.5. Crud Helper](#65-crud-helper)
- [7. Web.Config](#7-webconfig)
- [8. Securities](#8-securities)
    - [8.1. XSS Protection](#81-xss-protection)
    - [8.2. IDOR Protection](#82-idor-protection)
    - [8.3. Http Only Cookies](#83-http-only-cookies)
    - [8.4. Custom Error Handling](#83-http-only-cookies)
- [9. Deployment](#9-deployment)
    - [9.1. Azure Deployment](#91-azure-deployment)
    - [9.2. Production Deployment](#92-production-deployment)
    - [9.3. Versioning Guide](#93-versioning-guide)
- [10. Contributing](#10-contributing)

### Overview ###

C Sharp Starter Pack dapat digunakan untuk base code/framework membuat aplikasi berbasis ASP .Net MVC 5. 
Dalam source ini sudah disertakan sample project pemesanan tiket

## Technical Documentation ##

### 1. Getting Started ###
Untuk memulai development project ini terdapat beberapa applikasi yang perlu diinstall terlebih dahulu :

- Visual Studio mininal 2017 [*ASP .Net and web development* enabled](https://docs.microsoft.com/en-us/visualstudio/install/modify-visual-studio?view=vs-2017)
- .Net framework 4.6.1
- PostgreSQL 11

#### 1.1. Code Setup ####
Apabila sudah terpenuhi kemudian ikuti langkah berikut :

- Clone repository ini
- Buka solution ```c-sharp-starter-pack\Boilerplate\App\App.sln```
- Pada window *Solution Explorer* klik kanan pada solution lalu pilih ``` Restore NuGet Packages ```

![](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/dev-1/docs/Solution.JPG)

![](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/RestorePackage.jpg)

- Kemudian ```Build > Build Solution ``` atau ``` F6 ```
- Tunggu proses build sambil berdoa agar proses build berjalan lancar :pray:  

#### 1.2. Database Setup ####
apabila build sudah berhasil, langkah berikutnya adalah menyiapkan database. berikut ini langkah langkah menyiapkan database :

- Buat database baru
- Buka Web.config
- ganti *value* pada section ```<connectionStrings>``` dengan name ```DefaultConnection``` sesuai database yang baru dibuat
- Buka menu ``` Tools > NuGet Package Manager > Package Manager Console ```
- Danti default project menjadi ``` DataAccess\App.DataAccesss```
- Pada console jalankan perintah ```Update-Database```
- Tunggu proses migrasi skema sampai selesai
- Jalankan aplikasi dengan menekan tombol ```F6``` atau melalui menu ```Debug > Start Debugging```
- Login sebagai admin dengan menggunakan user ```admin@app.net``` dan password ```Aspapp@789```
Happy Developing :metal:

#### 1.3. Sample Project
Sample project yang sudah include pada project ini adalah contoh aplikasi sederhana order tiket bioskop. untuk memudahkan pemahaman penggunaan framework ini. dapat difork dari branch ```master```

#### 1.4. Bare Project
Apabila tidak membutuhkan sample atau hanya base code saja, dapat meng-fork dari branch ```bare-master```

### 2. Project Structure ### 
Dokumentasi mengenai struktur/arsitektur aplikasi aplikasi. Struktur project aplikasi terbagi menjadi :

- Data Access
- Integration
- Utilities
- Website

![enter image description here](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/Architecture.jpg)

Secara umum project ini dibuat dengan ASP .Net MVC 5 dengan target framework .Net 4.6.1

#### 2.1. Data Access ####
Data access berisi model database dan skema migrasi menggunakan Entity Framework 6.2.0 sebagai ORM Code First dan Npgsql 4.0.2 sebagai driver ke PostgreSQL.  

- Folder Identity berisi model bawaan dari ASP Identity
- Folder Migrations berisi skema migrasi yang akan dieksekusi apabila memulai dari database baru atau menambah/mengubah model
- Folder Models berisi model yang dipakai pada modul-modul aplikasi dan dibagi kedalam folder sesuai modul  

Seluruh model yang dibuat terkait proses bisnis aplikasi sangat disarankan untuk meng-extends dari ```ModelBase``` yang sudah dilengkapi dengan field-field basic dan ID menggunakan string GUID.  

#### 2.2. Integration ####
Secara umum, Integration berfungsi untuk melakukan integrasi data dengan project diluar aplikasi

- ```App.Synchronizer.DataServices``` Berisi logic untuk menginput data yang diterima keadalam database.  

#### 2.3. Utilities
Berisi modul sebagai bagian base dari framework aplikasi yang secara umum berisi helper method yang dapat digunakan pada layer selain ```DataAccess```.

#### 2.4. Web App ####
Project App.Web merupakan project yang berisi presentation layer dan business layer dari project ini.

Secara general untuk proses development hanya menyentuh folder **Areas** dan **Services**. namun terdapat juga beberapa fungsi lain yang dapat ditambahkan atau digunakan dalam folder **Utilities** dan **assets**  

#### 2.4.1. Areas ####
Folder Areas berisi view, view model dan controller yang terbagi kedalam modul-modul. dalam direktori ini lebih banyak dilakukan proses development untuk mengatur arus data dari view ke controller dan controller ke view. sehingga seminimal mungkin logic bisnis dilakukan di sini.

#### 2.4.2. Services ####
Bagian ini berisi business logic aplikasi dimana pada umumnya data dari atau ke controller diproses dalam Services. akses ke DataAccess sebaiknya dilakukan sepenuhnya di sini.

Folder ini terdiri dari 2 sub folder yaitu Contracts dan Repositories.

Contracts berisi Interface class yang nantinya akan dipanggil di Controller atau di Repositories lainnya. Interfaces ini akan diimplement di Class yang berada di folder Repositories. proses dependency injection interface dengan class yang mengimplement interface tersebut menggunakan plugin dependency injection **Ninject**.

#### 2.4.3. Utilities ####
Berisi *helper* untuk memudahkan proses-proses umum di aplikasi, Base Class, Custom Attribute dan [Action Filter](https://www.c-sharpcorner.com/article/filters-in-Asp-Net-mvc-5-0-part-twelve/)

Dalam folder ini juga terdapat sub folder *Settings* yang berisi setting untuk melakukan render template field-field input di aplikasi.

#### 2.4.4. assets ####
File custom yang terdapat dalam folder ini adalah dan ```assets/app/js/my-script.js``` dan ```assets/app/style/App.css``` merupakan file yang berisi helper dan style umum untuk view

### 3. Plugins ###
aplikasi menggunakan beberapa plugins dengan spesifikasi dan penjelasan sebagai berikut :

#### 3.1. Entity Framework ####
Digunakan sebagai ORM pada DataAccess dan Integrasi DSAS

#### 3.2. Npgsql ####
Digunakan sebagai data provider untuk PostgreSQL

#### 3.3. Hangfire ####
Digunakan untuk me-manage *background job* dan *cron job*. Hangfire Dashboard dapat diakses pada url ```/hangfire{App:HangfireSecret pada web.config}``` oleh **user yang diberikan hak akses background job**

contoh :

```/hangfireka1cg99ha5e8b3qssgq1q3dbg9ubh3oen6ibic2m8z8plo7hi04yk3ti4unhpts3```

untuk menambahkan cron job baru sebaiknya dilakukan dengan cara melakukan inherit interface ```IJobService```

```cs
public  interface  INewCronJobServices : IServiceBase, IJobService {

}
```

kemudian pada class implementasinya :

```cs
public  class  NewCronJobServices : ServiceBase, INewCronJobServices {
    public  void  StartWorker() {
        RecurringJob.AddOrUpdate("NewCronJob", () => DistributeReminder(true),"1 0 * * *",TimeZoneInfo.Local);
    }
}
```

kemudian daftarkan interface ```INewCronJobServices ``` ke class ```App_Start\CronJobConfig.cs```

```cs
public  void  StartCronJobs() {

    StartJob<IPelaporanEvaluasiJobServices>();
    StartJob<IDSASJobService>();
    StartJob<INotasiKhususJobServices>();
    .
    .
    StartJob<INewCronJobServices>();
}
```

> Interface class INewCronJobServices perlu didaftarkan pada ```App.Web.Services.ApplicationNinjectModule.cs``` , dapat dilihat pada sub judul [2.4. Ninject](#24-ninject).

#### 3.4. Ninject ####
Digunakan untuk melakukan dependency injection. setting binding dependency injection dilakukan pada file ```App.Web.Services.ApplicationNinjectModule.cs``` dalam method ```Load()```

```cs
public  override  void  Load() {
    Kernel.Bind<INewCronJobServices>().To<NewCronJobServices>().InAppScope();
    Kernel.Bind<INewServices>().To<NewServices>().InAppScope();
    .
    .
    .
}
```
penggunaan Dependency injection di aplikasi kebanyakan dilakukan melalui constructor injection dengan contoh penggunaan sebagai berikut :
```cs
public  class  YourServices : ServiceBase, IYourServices {
    private  IExistingServices  _existingSvc;
    private  IYourOtherNewServices  _otherNewSvc;
    public  YourServices (IExistingServices  existingSvc, IYourOtherNewServices  otherNewSvc){
        this._existingSvc= existingSvc;
        this._otherNewSvc = otherNewSvc;
    }
}
```
initial setup kernel Ninject dilakukan di ```App_Start\Ninject.Web.Common.cs```

Dokumentasi Ninject lebih lanjut dapat dilihat [di sini](https://github.com/ninject/ninject/wiki).

#### 3.5. OpenXML ####
Digunakan untuk melakukan proses read/write file .xlsx dan proses generate dokumen .docx.
Contoh penggunaan dapat dilihat pada service-service berikut

##### 3.5.1. Excel Processing #####
OpenXML dapat digunakan untuk melakukan proses read atau write 

##### 3.5.2. Word Processing #####
Penggunaan OpenXML untuk melakukan generate file .docx dengan cara membaca template kemudian me-replace tag-tag dalam template sudah di-wrap dalam service ```WordTextReplacementServices.cs``` namun apabila terdapat requirement untuk meng-inject tabel atau konten dengan jumlah yang dinamis semacamnya perlu dilakukan secara manual. Berikut contoh penggunaan word processing :

###### 3.5.2.1. Text Replacement #####
Tambahkan file template kedalam folder ```App_Data\DocTemplate``` contoh ```App_Data\DocTemplate\Samples\NewTemplate.docx``` kemudian daftarkan file tersebut ke ```WordReplacement.cs``` sebagai const

```cs
namespace  App.Web.Models {

    public  class  WordReplacement {

        public  const  string  TICKET = "Samples\\Ticket.docx";
    }
}
```

Hal ini dilakukan agar penempatan template aplikasi terpusat dan dapat dimanage dengan baik dan terdokumentasi.

Template

![enter image description here](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/TemplateDocx.JPG)

Kemudian untuk melakukan generate menggunakan template tersebut dilakukan dengan cara berikut

```cs
public  class  YourServices : ServiceBase, IYourServices {

    //instance word text replacement service
    private  IWordTextReplacementServices  _wordSvc;

    //contructor dependency injection
    public  YourServices (IWordTextReplacementServices  wordSvc){
        this._wordSvc = wordSvc;
    }

    public  FileModel  GetDoc(string  id){
        var  replaceItem = new  List<WordReplacement>();
        replaceItem.Add(new  WordReplacement("#Name", "John Doe"));
        replaceItem.Add(new  WordReplacement("#Date", DateTime.Now.ToString("dd/MM/yyyy hh:mm") ?? ""));
        //meng-inject number dengan thousand separato menggunakan helper ToFormatedString
        replaceItem.Add(new  WordReplacement("#Number", 10000000.ToFormatedString()?? ""));
        //text replacement dengan pemecahan per karakter
        replaceItem.AddRange(WordReplacement.BreakByChar("ABCDE-GHIJ", 10, "P{0}"));

        var  result = new  FileModel() {
            //melakukan replace dan menghasilkan binary atau byte[]
            Bytes = _wordSvc.GetWordReplacedText(WordReplacement.YOUR_NEW_TEMPLATE, replaceItem),
            FileName = "Result.docx",
            Mime = FileModel.MIME_DOCX
        };
    }
}

```

Hasil
![enter image description here](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/ResultDocx.JPG)


#### 3.6. AutoMapper ####
AutoMapper secara umum digunakan untuk melakukan mapping secara otomatis property object Model ke ViewModel atau sebaliknya.
Agar AutoMapper dapat digunakan untuk melakukan mapping antar tipe class, terlebih dahulu harus dilakukan pendaftaran atau mapping tipe di file ```App_Start\AutoMapperConfig.cs``` pada method ```RegisterMapper```
```cs
private  static  void  RegisterMapper(IMapperConfigurationExpression  cfg) {
    .
    .
    .
    cfg.CreateMap<YourModelClass, YourViewModelClass>().ReverseMap();
    cfg.CreateMap<YourOtherModelClass, YourOtherViewModelClass>().ReverseMap()
    .ForMember(t => t.Relation1, opt => opt.Ignore())
    .ForMember(t => t.Relation2, opt => opt.Ignore());
}

```

Contoh penggunaan ```AutoMapper``` yang menghasilkan object baru

```cs
using  AutoMapper;
.
.
var  model = GetModelFromDB(id);
//mapping model menjadi object viewmodel baru
var  result = Mapper.Map<ViewModel>(model);
```

Contoh penggunaan ```AutoMapper``` untuk mapping ke *existing object*

```cs
using  AutoMapper;
.
.
void  UpdateData(string  id, ViewModel  viewModelObj){
    var  model = GetModelFromDB(id);
    model = Mapper.Map(viewModelObj,model);
    context.SaveChanges();
}
```

>  **Warning !** Penggunaan automapper untuk melakukan proses update data dari model harus dilakukan dengan cara mapping ke *existing object* agar Entity Framework dapat mendetect perubahan pada field.

>  **Warning !** perlu ditambahkan ```.ForMember(t => t.Relation1, opt => opt.Ignore()``` pada mapping field-field reference atau relasi agar ketika dilakukan proses insert/update, Entity Framework tidak membuat objek relasi baru ketika ```Mapper.Map()``` dipanggil.

Panduan penggunaan AutoMapper selengkapnya dapat dilihat [disini](https://automapper.readthedocs.io/en/latest/).

#### 3.7. Elmah ####
Elmah dalam project aplikasi digunakan untuk melakukan error tracking. dalam proses development, error log dapat dilihat melalui url ```/{elmah.mvc.route pada web.config}```

atau untuk environment localhost : ```/elmah-21c38dfdd8b549c8a24ba3c0484bfb34ba65b3330c6b4c8cafe43c506495c50a2cf8cb1c8ed447f49f6aebd11378439a```
file-file hasil logging disimpan di ```App_Data/Elmah.Errors```

untuk saat ini elmah **hanya dapat diakses melalui localhost** karena belum terotorisasi menggunakan login sehingga memunculkan temuan pentest.

#### 3.8. Swagger ####
Swagger berfungsi untuk mendokumentasikan API yang tersedia di project App.Web. halaman swagger dapat diakses melalui url ```/swagger```

### 4. Development ###
Proses development fitur kebanyakan akan dilakukan dalam project DataAccess untuk mengubah skema Database dan Web untuk logic dan view. 

> **Tips** Untuk memudahkan melakukan navigasi melihat isi sebuah method, gunakan kombinasi keyboard <kbd>ctrl</kbd> + <kbd>f12</kbd> atau klik kanan ```Go To Implementation``` agar diarahkan ke implementasi dari method tersebut, bukan ke interface nya

#### 4.1. Menambahkan dan Mengubah Model ####
##### 4.1.1. Menambahkan Model #####
- Menambahkan model dapat dilakukan dengan cara menambahkan class baru pada ```App.DataAccess\Model\{Nama folder sesuai modul}``` yang meng-extends atau inheit class ```ModelBase``` 
```cs
public class MyNewModel : ModelBase {
    public int Number { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }
    public OtherModel OtherModel { get; set; }
}
```
- kemudian class tersebut didaftarkan sebagai property di class ```ApplicationDbContext```
```cs
public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim> {
    .
    .
    .
    
    public DbSet<ParameterARInstrument> ARInstruments { get; set; }
    public DbSet<ParameterARBoard> ARBoards { get; set; }
    public DbSet<MyNewModel> MyNewModels { get; set; }
}
```
> **Info** Sebaiknya nama property pada ```ApplicationDbContext``` ditulis dengan penaman *prular nouns*

##### 4.1.2. Mengubah Model #####
Mengubah model dapat langsung dilakukan dengan cara mengedit property model kemudian melakukan migrasi schema

##### 4.1.3. Schema Migration #####
- Setelah itu buka NuGet Package manager melalui menu ```Tools > NuGet Package Manager > Package Manager Console``` 
- Pilih Default Project ```DataAccess\App.DataAccess``` pada dropdown
- Jalankan perintah ```Update-Database``
- Kemudian ```Add-Migrations {migration_message}``` isikan migration message dengan pesan yang informatif. 
- Kemudian jalankan lagi ```Update-Database```

berikut ini best practices untuk mengisi migration message
    - ```add_{nama_model}``` apabila menambahkan model 
    - ```delete_{nama_model}``` apabila menghapus model 
    - ```add_field_{nama_field}_{nama_model}``` apabila menambahkan 1 field pada model 
    - ```delete_field``` apabila menghapus 1 field pada model 
    - ```update_field``` apabila menghapus 1 field pada model 
    - ```update_model_{nama_model}``` apabila mengubah lebih dari 1 field pada model

##### 4.1.4. Menambahkan View Model #####
- Menambahkan model dapat dilakukan dengan cara menambahkan class baru pada ```App.Web\Areas\{Nama folder sesuai modul}\Model\``` yang meng-extends atau inheit class ```ViewModelBase```
```cs
private  class MyNewModelForm : ViewModelBase {
    public int Number { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }
    public OtherModel OtherModel { get; set; }
}

```
- Tambahkan mapper sesuai model yang dibuat view modelnya di ```App_Start\AutoMapperConfig.cs``` pada method ```RegisterMapper```
```cs
private static void RegisterMapper(IMapperConfigurationExpression  cfg) {
    .
    .
    .
    cfg.CreateMap<MyNewModel, MyNewModelForm>().ReverseMap()
                .ForMember(t => t.OtherModel, opt => opt.Ignore());
}
```

#### 4.2. Menambahkan dan Mengubah Service ####
Services berisi logic aplikasi sehingga proses development akan lebih banyak mengubah service. berikut ini adalah beberapa panduan mengubah services.

##### 4.2.1. Menambahkan Service #####
- Tambahkan interface class pada ```App.Web\Services\Contracts\``` dengan penamaan file ```I{NamaServices}Services``` dan menginherit ```IServiceBase``` 
```cs
public interface IMyNewServices : IServiceBase {
        void DoSomething(MyNewModelForm form);
        List<MyNewModelForm> GetList();
}
```
- Tambahkan class pada ```App.Web\Services\Repositories\``` dengan penamaan file ```{NamaService}Services``` dan menginherit ```ServiceBase``` dan meng-implement interface yang dibuat pada step sebelumnya
```cs
public class MyNewServices : ServiceBase, IMyNewServices {
        public void DoSomething(MyNewModelForm form){ ... }
        public List<MyNewModelForm> GetList() { ... }
}
```

- Binding dependency injection pada file ```App.Web.Services.ApplicationNinjectModule.cs``` dalam method ```Load()```
```cs
public override void Load() {
    .
    .
    .
    Kernel.Bind<IMyNewServices>().To<MyNewServices>().InAppScope();
}
```

##### 4.2.2. Menambahkan Method #####
Pada dasarnya menambah method pada service hampir sama dengan ketike menambah service baru, yaitu semua method pada interface harus diimplementasikan

```cs
public interface IMyNewServices : IServiceBase {
        void DoSomething(MyNewModelForm form);
        List<MyNewModelForm> GetList();
        .
        .
        void MyNewMethod();
}
```

```cs
public class MyNewServices : ServiceBase, IMyNewServices {
        public void DoSomething(MyNewModelForm form){ ... }
        public List<MyNewModelForm> GetList() { ... }
        .
        .
        void MyNewMethod(){ ... }
}
```

##### 4.2.3. Menambahkan Services Dependency #####
- Buat private field dengan tipe interface service yang ingin digunakan
- Buat constructor dengan tipe parameter interface yang ingin kita gunakan
- assign private field tersebut dengan parameter dari constructor
```cs
public class MyServices : ServiceBase, IMyServices {
    //private field untuk menampung instance service
    IEmailServices _emailSvc;

    //constructor dependency injection
    public MyServices(IEmailServices emailSvc){
        this._emailSvc = emailSvc;
    }

    public void DoSomething(MyNewModelForm form){
        //memanggil method pada service EmailServices
        _emailSvc.SendEmail(form.Email,form.Body);
    }
}
```

**Warning !** apabila 2 service saling bergantung satu sama lain, akan terjadi error ```circular dependancy```. contoh :
```cs
public class MyServices : ServiceBase, IMyServices {
    IEmailServices _emailSvc;

    public MyServices(IEmailServices emailSvc){
        this._emailSvc = emailSvc;
    }
}

public class EmailServices : ServiceBase, IEmailServices {
    IMyServices _mySvc;

    public MyServices(IMyServices mySvc){
        this._mySvc = mySvc;
    }
}
```

##### 4.2.4. Mengenai ServiceBase #####
Beberapa method dan property ServiceBase yang dapat digunakan untuk membantu proses development :
- Property ```context``` adalah database context yang dapat digunakan untuk melakukan proses I/O database
- Property ```Tomorrow``` mendapatkan tanggal esok hari **sesuai hari kerja** tanpa jam
- Property ```Yesterday``` mendapatkan tanggal kemarin **sesuai hari kerja** tanpa jam
- Method ```GetUserByEmail(string email)``` mendapatkan data user sesuai email/username
- Method ```GetCurrentUser()``` mendapatkan data user yang sedang login
- Method ```ErrorLog(Exception e)``` melakukan log error ke Elmah

#### 4.3. Menambahkan dan Mengubah Controller ####
Controller secara umum menghandle data dari Service ke View atau sebaliknya. berikut ini cara menambahkan dan mengubah controller

##### 4.3.1. Menambahkan Controller #####
- Untuk menambahkan controller pada suatu modul klik kanan pada ```App.Web\Areas\{nama modul}\Controllers\``` jika tidak ada maka tambahkan di folder ```App.Web\Controllers\```
- klik ```Add > Controller...```
- Beri nama controller
- ganti inheritance controller dari ```Controller``` menjadi ```BaseController``` dengan mengimport namespaces ```App.Web.Utilities```
```cs
public class MyNewController : Controller {

}
```
menjadi 
```cs
using App.Web.Utilities;
public class MyNewController : BaseController {

}
```

##### 4.3.2. Menambahkan Service #####
Pada dasarnya untuk menambahkan service dilakukan dengan cara yang sama dengan menambahkan service dalam service
- Buat private field dengan tipe interface service yang ingin digunakan
- Buat constructor dengan tipe parameter interface yang ingin kita gunakan
- assign private field tersebut dengan parameter dari constructor
```cs
public class MyNewController : BaseController {
    //private field untuk menampung instance service
    IMyServices _mySvc;

    //constructor dependency injection
    public MyNewController(IMyServices mySvc){
        this._mySvc = mySvc;
    }

    public ActionResult DoSomething(MyNewModelForm form){
        //memanggil method pada service EmailServices
        _emailSvc.SendEmail(form.Email,form.Body);
        return Redirect("Home");
    }
}
```

##### 4.3.3. Mengenai BaseController #####
Beberapa method dan property BaseController yang dapat digunakan untuk membantu proses development :
- method ```Json()``` adalah hasil override method Json() bawaan ASP .Net MVC untuk melakukan return data berbentuk JSON dengan menggunakan json parser NewtonSoft untuk menghindari circular reference dan memformat date menjadi format ISO
- method ```File(FileModel model)``` helper untuk melakukan return file (download)
- method ```InlineFile(FileModel model)``` untuk melakukan return file inline (misal bisa untuk gambar, pdf atau video)
- method ```SetActivityLogData``` untuk melakukan logging aktivitas user, fitur ini akan dijelaskan lebih dalam di Helper Log Attribute
- method ```ErrorLog(Exception e)``` melakukan log error ke Elmah

##### 4.3.4. Flash Message #####
Digunakan untuk menampilkan message setelah action dilakukan sehingga menyebabkan halaman dirender ulang. penggunaan flash message dapat dilakukan pada ```ActionResult``` dengan mengimport library ```App.Idx.Utilities```. contoh penggunaan flash message :
```cs
public ActionResult DoSomething(MyNewModelForm form){
    return Redirect("Home").Success("Do Something"); // menampilkan flash message : Berhasil, Do Something
    return Redirect("Home").Warning("Do Something");// menampilkan flash message : Peringatan, Do Something
    return Redirect("Home").Error("Do Something");// menampilkan flash message : Error, Do Something
    return Redirect("Home").Information("Do Something");// menampilkan flash message : Info, Do Something
}
```

#### 4.4. Menambahkan dan Mengubah View ####

##### 4.4.1. Menambahkan View #####
Menambahkan view pada project ini sama dengan cara menambahkan view ASP .Net pada umumnya. view yang ditambahkan dalam folder ```Areas``` secara otomatis akan menggunakan layout master dan mengimport library-library helper

##### 4.4.2. Menambahkan Style #####
Menambahkan style dalam view dilakukan dengan didalam tag ```@using (Html.Section("styles")) { }``` agar style di-render pada ```<head>``` section
```html
@using (Html.Section("styles")) { 
    <style>
        .myclass {
            width: 20%;
            float: left
        }   
    </style>
}
```

##### 4.4.3. Menambahkan Script #####
Menambahkan style dalam view dilakukan dengan didalam tag ```@using (Html.Section("scripts")) { }``` agar style di-render pada ```<body>``` sesuai urutan import script
```html
@using (Html.Section("scripts")) { 
    <script>
        //do some JS  
    </script>
}
```

##### 4.4.4. FormFieldPartial #####
Untuk memudahkan proses development, terdapat method Html helper custom untuk merender form controls yaitu ```@Html.FormFieldPartial()```
Method ini menerima argumen model dan konfigurasi
method ini akan melakukan render form control sesuai dengan tipe class yang dimasukan pada konfigurasi
- ```TextFieldSetting``` Untuk merender input text field , password dan textarea
- ```DecimalFieldSetting``` untuk merender input number
- ```DateTimeFieldSetting``` untuk merender ```datepicker```
- ```RadioFieldSetting``` untuk merender radio button
- ```SelectFieldSetting``` untuk merender ```<select>```
- ```ChecklistFieldSetting``` untuk merender check box

```cs
//Text field
@Html.FormFieldPartial((Model.BoardForm?.Board ?? "").ToString(), new TextFieldSetting() {
    InputName = Html.NameFor(t => t.BoardForm.Board).ToString(),
    Label = Html.LabelForRequired(e => e.BoardForm.Board).ToString(),
    HelpText = "",
    MaxLength = 5,
    Placeholder = "Board",
    CssClass = "",
    Style = TextFieldStyle.Simple
})

//Decimal field
 @Html.FormFieldPartial(Model?.BoardForm?.Value, new DecimalFieldSetting() {
    InputName = Html.NameFor(t => t.BoardForm.Value).ToString(),
    Label = Html.LabelForRequired(e => e.BoardForm.Value).ToString(),
    HelpText = "",
    Placeholder = "Decimal Point Price",
    CssClass = "",
    HtmlId = "decimal-input"
})

//Select
@Html.FormFieldPartial((Model?.BoardForm?.BoardGroup?.Id ?? "").ToString(), new SelectFieldSetting() {
    InputName = Html.NameFor(t => t.BoardForm.BoardGroupId).ToString(),
    Label = Html.LabelForRequired(e => e.BoardForm.BoardGroupId).ToString(),
    HelpText = "",
    Placeholder = "Board Group",
    CssClass = "",
    Options = Model.BoardGroups,
    OptionLabelGetter = t => t.DirectCastTo<BoardGroup>().BoardGroupCode,
    OptionValueGetter = t => t.DirectCastTo<BoardGroup>().Id
})
```

contoh lebih lanjut dapat dilihat di view yang sudah ada





#### 4.5. Menambahakan Areas
Areas bermanfaat untuk memisahkan controller , views dan model kedalam beberapa folder agar lebih rapi. berikut ini cara menambahkan area baru

1. Klik kanan pada folder ```App.Web\Areas\``` > ```Add``` > ```Area...```
2. Isikan nama area, contoh : Test
3. Maka akan muncul folder ```App.Web\Areas\{NamaArea}```
4. Buka file ```App.Web\Areas\{NamaArea}\{NamaArea}AreaRegistration.cs```. kemudian edit menjadi seperti berikut :
```cs
using App.Web.Utilities;
public class TestAreaRegistration : AreaRegistration 
{
    public override string AreaName 
    {
        get 
        {
            return "Test";
        }
    }

    public override void RegisterArea(AreaRegistrationContext context) 
    {
        this.RegisterDefaultAreaRoute(context);
    }
}
```
5. Tambahkan file baru ```App.Web\Areas\{NamaArea}\Views\_ViewStart.cshtml``` dengan isi 
```cs
@{
    Layout = "~/Views/Shared/Metronic/_Layout.cshtml";
}
```

6. Buka file ```App.Web\Areas\{NamaArea}\Views\web.config``` kemudian tambahkan line berikut dibawah tag ```<namespaces>: 
```xml
<add namespace="App.DataAccess" />
<add namespace="App.Web.Utilities" />
<add namespace="App.Utilities" />
<add namespace="App.Web.Areas.{NamaArea}.Model" />
<add namespace="App.Web.Utilities.Settings" />
<add namespace="App.Web.Utilities.CustomValidationSummary" />
```
Silahkan tambahkan namespaces lain apabila dirasa perlu

7. Area siap digunakan

### 5. Available Services ###
Beberapa services yang sudah tersedia dalam project dan sering digunakan untuk memudahkan proses development 

#### 5.1. IEmailServices ####
Digunakan untuk mengirimkan email
- ```bool SendEmailICS(string[] emailDestination, string subject, string body, ICSFileModel icsFileModel);``` mengirimkan Email dengan alternate view ICS atau kalender. dapat digunakan untuk reminder event atau invitation
- ```bool SendEmailOnly(string[] emailDestination, string subject, string body);``` mengirimkan hanya email tanpa kalender

#### 5.2. IExcelFileServices ####
Digunakan untuk memudahkan pengolahan file excel (.xlsx)
- ```FileStream GetWorksheetFileStream(string templatePath);``` membaca stream template excel, dapat digunakan untuk generate file excel dari template. parameter templatePath diisi dengan path relative dari settingan ```doctemplate``` pada ```Web.config```
- ```ReadExcelData``` membaca data pada file excel dan menghasilkan ```List<dynamic>``` dengan nama property sesuai header kolom pada file excel

#### 5.3. IFileManServices ####
Digunakan untuk memudahkan dan memusatkan proses pengelolaan I/O file
- ```FileModel SaveFile(FileModel file);``` Digunakan untuk menyimpan file ke ```FileStorage``` sesuai setting pada ```Web.Config```atau ```absolute path```
- ```FileModel LoadFile(string filename); ``` Digunakan untuk membaca file ke ```FileStorage``` sesuai setting pada ```Web.Config``` atau ```absolute path```
- ```Task<FileModel> DeleteFileAsync(string filename); ```Digunakan untuk menghapus file ke ```FileStorage``` sesuai setting pada ```Web.Config``` atau ```absolute path```
- ```string GetDocStoragePath(); ``` Digunakan untuk membaca path DocStorage yang diatur di ```Web.Config```
- ```string GetDocTemplatePath(); ``` Digunakan untuk membaca path DocTemplate yang diatur di ```Web.Config```
- ```void WriteText(string content, string path); ``` Digunakan untuk menulis text menjadi sebuah file

#### 5.4. IMigrasiDataServices ####
Services ini berfungsi untuk melakukan migrasi data. apabila selama proses development diperlukan migrasi data dalam bentuk apapun. sebaiknya dilakukan dengan membuat script migrasi dalam services ini agar metode migrarsi terdokumentasi dengan lebih baik. menambahkan cara menambahkan method untuk migrasi data sama dapat dilihat [disini](#42-menambahkan-dan-mengubah-service)

#### 5.5. IWordTextReplacementServices ####
Digunakan untuk melakukan pengolahan file .docx
- ```byte[] GetWordReplacedText(string templatePath, List<WordReplacement> items);``` sangat sering digunakan, untuk membaca template dan me-replace tag-tag yang didefinisikan kedalam ```List<WordReplacement>``` selengkapnya dapat dibaca [di sini](#352-word-processing)

#### 5.6. IWorkflowServices ####
Digunakan untuk membuat workflow yang disertakan reminder atau notifikasi. atau hanya untuk membuat notifikasi
- ```List<ToDoForm> GetToDoList(DateTime? date = null);``` Mengambil data to do list hari ini
- ```FlowForm SetStatus(string Id, string status); ``` Mengubah status sebuah flow berdasarkan ID
- ```Workflow Upsert(IEnumerable<Flow> flows, string dataId, aplikasiReminderTypes type); ``` melakukan upsert (insert bisa tidak ada , update bila ada) flow atau reminder berdasarkan ID
- ```List<Flow> GetFlowByDataId(string dataId, aplikasiReminderTypes type); ``` mengambil sebuah flow berdasarkan ID nya

### 6. Helper ###
Selain service, terdaoat juga beberapa helper yang sering digunakan dalam project untuk memudahkan proses development. helper dapat digunakan dengan cara mengimport namesapace ```App.Utilities```
berikut ini beberapa helper yang sering digunakan :


#### 6.1. Static Method Helper ####
Static method helper digunakan dengan sintax ```CommonHelper.{NamaMethod}``` contoh ```CommonHelper.GetUser()```
- ```GetVersion()``` digunakan untuk menampilkan versi aplikasi. apabila ingin menaikan versi aplikasi silahkan ubah nilai return dari method ini
- ```GetUser()``` untuk mendapatkan data user yang sedang login
- ```GetUserByEmail(string email)``` untuk mendapatkan data user berddasarkan email
- ```GetExcelColumnName(int columnNumber)``` untuk mengkonversi angka menjadi nama column excel. (ex : 10 menjadi J atau 27 menjadi AA)
- ```GetUserByEmailOrUserName(string key)``` digunakan untuk mendapatkan data user berdasarkan email atau username

#### 6.2. Extension Method Helper ####
Extension method digunakan pada objek dengan tipe data sesuai deklarasi ``` this ``` pada parameter pertama. contoh ```"DateTime.Now.ToStringIndonesia("dddd, dd MMMM yyyy")```
- ```SafeRound(this decimal val, int decimalplace)``` untuk melakukan pembulatan secara aman*
- ```SafeGetElementAt<T>(this T[] array, int index)``` digunakan untuk mengambil elemen pada array secara aman*
- ```GetActiveData<T>(this IQueryable<T> model)``` digunakan untuk menambahkan kondisi ```IsDeleted != true && IsDraft == falase``` pada query object yang meng-inherit ```ModelBase```
- ```SafeConvert<T>(this string s)``` mengkonversi string secara aman menjadi object dengan tipe data yang diinginkan
- ```ToPascalCase(this string s, char separator = ' ')``` mengubah string menjadi pascalcase sesuai separator contoh : ```"john doe".ToPascalCase()``` akan menghasilkan ```John Doe```, ```"john_doe".ToPascalCase('_')``` akan menghasilkan ```John_Doe```
- ```SafeDateConvert(this string s, string format = null) ``` melakukan konversi string menjadi datetime secara aman*
- ```SafeDecimalConvert(this string s) ``` melakukan konversi string menjadi decimal secara aman*
- ```ToFormatedString(this decimal number, bool addRupiah = false, Separator separator = Separator.Titik, int? decimalPoint = null, string zeroReplacement = null) ``` mengkonversikan decimal menjadi string yang sudah diformat dengan thousand separator
- ```Round(this decimal? number, int decimalplace = 3) ``` melakukan pembulatan decimal
- ```AddWorkDays(this DateTime originalDate, int workDays) ``` menambahkan hari sesuai settingan hari kerja
- ```IsHoliday(this DateTime originalDate, string module = null) ``` mengecek apakah sebuah tanggal adalah hari libur sesuai settingan hari kerja
- ```ToStringIndonesia(this DateTime date, string format) ``` sama dengan ```ToString``` pada ```DateTime``` tapi dalam bahasa Indonesia
- ```GetDoublePrecision(this double? source, bool useIdCulture = false) ``` Convert string sesuai jumlah inputan user berapa angka dibelakang koma dan meniadakan angka di belakang koma jika user tidak input angka di belakang koma

> **Note** *) aman = apabila terjadi ```error exception``` maka method akan menghasilkan nilai ```null```

#### 6.3. Attribute Helper ####
Digunakan dengan cara menambahkan attribute pada class, property atau method. contoh 
```cs
[Log("Save Data")]
public ActionResult Entry(BoardViewModel vm) { . . . }
```

- ```Log``` digunakan pada Action dalam controller. untuk melakukan logging activity. contoh penggunaan attribute ```Log```
```cs
// aktivitas ini akan di-log dengen keterangan "Save Data"
[Log("Save Data")]
public ActionResult Entry(BoardViewModel vm) { 
    // menambahkan data untuk dilog
    SetActivityLogData(vm.Name);    
    . . . 
}
```
> **Info** method ```SetActivityLogData``` hanya tersedia apabila controller menggunakan parent class ```BaseController```
- ```AppAuth``` digunakan pada Action atau controller untuk membatasi akses user. contoh penggunaan attribute ```AppAuth```
```cs
[AppAuth]
public class BoardController : BaseController
{
    ...
}
```
dengan demikian hanya user yang sudah login yang dapat mengakses controller ```BoardController```

- ```Module``` digunakan untuk menandai sebuah controller masuk kedalam modul apa. penggunaan modul erat kaitannya dengan ```Log``` yaitu untuk memberikan data pada kolom ```Modul``` pada activity log. contoh : 
```cs
[Modul(Name = AppModule.Samples_ticket)]
public class TicketController : BaseController
{
    [Log("Visit Page")]
    public ActionResult Index(){
        ...
    }
}
```
maka nama module yang masuk dalam activity log adalah ```Samples_ticket``` dengan action ```Visit Page```

#### 6.4. Datatable Helper ####
Untuk memudahkan dan menstandard kan penggunaan ```jquery datatable```, Datatable helper dapat digunakan dengan panduan berikut ini :
##### 6.4.1. Menyiapkan View Model #####
Pada dasarnya helper akan melakukan render seluruh kolom dengan ```primitive data type``` dan menggunakan nama property sebagai header kolom. 
Dengan beberapa aturan tambahan menggunakan attribute sebagai berikut : 

- ```[Display]``` Apabila terdapat attribute tersebut maka header kolom akan menggunakan display yang ditentukan pada attribute tersebut
```cs
[Display(Name = "Security Code")]
public string SecurityCode { get; set; }
```
dengan settingan property seperti diatas. maka helper akan me-render kolom dengan header ```Security Code```

- ```[DttField]``` digunakna untuk mendefinisikan setting custom. attribute ini memiliki beberapa parameter constructor. berikut beberapa contoh umum penggunaan attribute ```[DttField]```
```cs
//menyembunyikan property dari datatable
[DttField(hidden:true)]
public string Property1 { get; set; }

//merender property nested object
[DttField(renderChilderen: new string[] { "OtherClassProperty1:Display Prop 1", "OtherClassProperty2.Code:Display Prop 2 Code" ...  })]
public OtherClass Property2 { get; set; }

//membuat field tidak dapat disearch
[DttField(searchable:false)]
public string Property3 { get; set; }

//membuat field tidak dapat diorder by
[DttField(orderable:false)]
public string Property4 { get; set; }

//membuat field tidak dapat diorder by
[DttField(orderable:false)]
public string Property4 { get; set; }

//membuat field dirender secara custom
[DttField(renderFunction:"customRender")]
public string Property5 { get; set; }
```

##### 6.4.2. Menyiapkan Services #####
penggunaan helper datatable pada service adaalah dengan cara memasukan object ```IQueryable``` dan model ```DttRequestWithDate``` pada method ```ProcessDatatableResult``` dengan type parameter pertama adalah tipe data ```View Model``` yang sudah disiapkan pada step sebelumnya, dan type parameter ke 2 adalah tipe model.
```cs
public DttResponseForm<MyModelForm> GetList(DttRequestWithDate form){
    return ProcessDatatableResult<MyModelForm, MyModel>(context.MyModel.Include("Property2"), form);
}
```
method ini akan menghasilkan ```DttResponseForm``` dengan generic type sesuai tipe data pada parameter pertama. dalam method ```ProcessDatatableResult``` data akan diproses sesuai ```paging``` , ```Ordering``` dan ```Dynamic Searching```

##### 6.4.3. Menyiapkan Action pada Controller #####
penggunaan datatable helper pada action cukup memanggil method pada service yang sudah disiapkan pada step sebelum ini. kemudian memanggil method ```Json``` dengan parameter hasil return dari service
```cs
[HttpPost]
public ActionResult List(DttRequestWithDate req) {
    return Json(_mySvc.GetList(req));
}
```

##### 6.4.4. Menyiapkan View #####
Penggunaan datatable helper pada view page 
```html
<!-- import header table yang berisi filter, column setting dan beberapa fitur lainnya -->
@Html.Partial("~/Views/Shared/Metronic/_TableToolsDynamic.cshtml")

<!-- buat tabel dengan id -->
<table class="table table-striped table-bordered table-hover table-checkable" id="mydatatable">
    <thead>
        <tr>
            <!-- memanggil helper untuk merender kolom sesuai model ChangeRemarksDailyForm-->
            @(Html.RenderColumnsHtml(typeof(App.Web.Areas.Equity.Models.MyModule.MyModelForm)))
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            <!-- memanggil helper untuk merender kolom -->
            @(Html.RenderColumnsHtml(typeof(App.Web.Areas.Equity.Models.MyModule.MyModelForm)))
        </tr>
    </tfoot>
</table>

@section scripts {
    <script>
        //memanggil helper untuk merender config column 
        var columns = @(Html.RenderColumnsOption(typeof(App.Web.Areas.Equity.Models.MyModule.MyModelForm)));

        $(document).ready(function () {
            //url adalah url action yang sudah disiapkan pada step sebelum ini, 
            DatatableInit($('#mydatatable'), { columnDefs: columns, url: '/Equity/MyModule/List', useNumberCol: true });
        });

        //custom render function dengan nama function sesuai dengan setting pada property
        function customRender(data, type, row) {

                return `<div class="custom">
                    ${row.Property1 + row.Property2}
                </div>`
        }

    </script>
}
```

##### 6.4.5. Kekurangan Datatable Helper #####
Penggunaan ```Datatable Helper``` sampai saat ini cukup membantu untuk mendeliver datatable view yang cukup lengkap. namun terdapat beberapa kekurangan yang perlu di-enhance dari helper ini

- Penggunaan yang belum ```Clean``` atau seamless. terutama pada helper untuk view. seharusnya deklarasi tabel dan script dapat dijadikan satu helper somehow
- Belum bisa meng-custom search dan order
- Belum bisa membuat custom view untuk filtering
- Export hanya bisa muncul seluruhnya apabila datatable dipilih menampilkan seluruh row

dengan pengembangan selanjutnya diharapkan kekurangan-kekurangan pada flow database helper dapat diatasi

#### 6.5. Crud Helper

Crud helper dapat digunakan untuk secara cepat membuat modul yang hanya digunakan untuk CRUD (Create Read Update Delete). berikut ini cara menambahkan Modul CRUD : 

1. Definisikan model pada ```App.DataAccess```. atau ikuti langkah [menambahkan model](#411-menambahkan-model)
```cs
public class Movie : ModelBase
{
    public string Name { get; set; }
    public string Category { get; set; }
    public string Language { get; set; }
    public DateTime? ReleaseDate { get; set; }
    public int Rating { get; set; }
    [JsonIgnore]
    public ICollection<ShowTime> ShowTimes { get; set; }
}
```

> **Info!** attribut JsonIgnore digunakan agar tidak terjadi *Self referencing loop* saat data diubah menjadi JSON

2. Buat form pada folder model dalam ```areas``` sesuai modul, dan meng-inherit ```FormBaseModel```. contoh. definisikan juga attribut-attribut yang diperlukan. contoh
```cs
public class MovieForm : FormModelBase
{
    [AppRequired]
    public string Name { get; set; }
    public string Category { get; set; }
    [DttField(renderFunction:"languageRender")]
    public string Language { get; set; }
    [AppRequired]
    [Display(Name = "Release Date")]
    public DateTime? ReleaseDate { get; set; }
    public int Rating { get; set; }
}
```
3. Buat view model dalam folder yang sama dengan form. dan menginherit ```ViewModelBase<T>``` dengan parameter ```T``` adalah form yang sudah dibuat. dalam view model ini juga dapat dideklarasikan field-field yang dapat digunakan untuk view ketika input data. contoh dalam kasus ini list of categories. dimana ini akan dimunculkan pada dropdown untuk memilih kategori film
```cs
public class MovieViewModel : ViewModelBase<MovieForm>
{
    public IEnumerable<string> Categories { get; set; }
}
```

4. Lakukan mapping pada ```App.Web\App_Start\AutoMapperConfig.cs```. sesuai [panduan ini](#36-automapper)
```cs
cfg.CreateMap<Movie, MovieForm>().ReverseMap()
    .ForMember(t => t.ShowTimes, opt => opt.Ignore());
```

5. Buat Interfce pada ```App.Web\Services\Contracts\``` dengan meng-inherit ```ICrudServices<TModel,TForm>``` dengan parameter ```TModel adalah model``` dan ```TForm adalah form```
```cs
public interface IMovieServices : ICrudServices<Movie,MovieForm>
{
    IEnumerable<string> GetCategories();
}
```
Dalam service ini dapat juga ditambahkan method custom untuk keperluan lain. dalam contoh ini ```GetCategories();``` yaitu untuk mengambil data categories

6. Buat class pada ```App.Web\Services\Contracts\``` dengan meng-inherit ```CrudServiceBase<TModel,TForm>```, dan meng-implemet interface ```IMovieServices``` yang barusan dibuat. berikut contoh nya
```cs
public class MovieServices : CrudServiceBase<Movie,MovieForm>, IMovieServices
{
    public MovieServices(ApplicationDbContext ctx, IMasterDataServices setupSvc, IWordTextReplacementServices wordSvc) 
        : base(ctx, setupSvc, wordSvc) { }


    public IEnumerable<string> GetCategories() {
        //dapat diganti dengan mengambil dari sistem lain, API atau database
        yield return "Sci-Fi";
        yield return "Comedy";
        yield return "Horror";
        yield return "Drama";
        yield return "Thriller";
        yield return "Biography";
    }
}
```

7. Buat class controller pada areas sesuai modul dengan meng klik kanan pada ```App.Web\Areas\{Nama Modul}\Controller``` kemudian pilih ```add``` > ```Controller...```. kemudian ubah inheritance controller menjadi ```BaseCrudController<TService, TViewModel, TModel, TForm>``` dengan parameter type ```TService``` adalah interface service, ```TViewModel``` adalah view model, ```TModel``` adalah model, ```TForm``` adalah Form.
```cs
public class MovieController : BaseCrudController<IMovieServices, MovieViewModel, Movie, MovieForm> {
    
        protected override MovieViewModel ViewModelLoader(MovieViewModel vm) {
            vm = base.ViewModelLoader(vm);

            vm.Categories = Service.GetCategories();

            return vm;
        }
}
```
mengoverride ```ViewModelLoader``` dilakukan untuk mengisi pilihan ```Categories``` dari service

8. Tambahkan halaman ``Index.cshtml`` dan ```Entry.cshtml``` dalam folder ```App.Web\Areas\{Nama Modul}\Views\Movie\``` folder movie dapat dibuat terlebih dahulu bisa belum ada. nama folder Movie diambil dari nama controller tanpa kata controller. dengan contoh berikut 

- Index.cshtml
```html

@{
    ViewBag.Title = "Movie";
    var controllerName = this.ViewContext.RouteData.Values["controller"].ToString();
    var areaname = ViewContext.RouteData.DataTokens["area"];
}



<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text"></h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="/@areaname/@controllerName/entry" class="btn btn-focus m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="fa fa-plus"></i>
                            <span>Add New</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>

    </div>
    <div class="m-portlet__body">

        @Html.Partial("~/Views/Shared/Metronic/_TableToolsDynamic.cshtml")
        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable">
            <thead>
                <tr>
                    @(Html.RenderColumnsHtml(typeof(MovieForm)))
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    @(Html.RenderColumnsHtml(typeof(MovieForm)))
                </tr>
            </tfoot>
        </table>
    </div>
</div>


@section scripts{
    @*<style>
            #override.fordynamic {
                display: none;
            }
        </style>*@

    <script>
        var dtListing = {};
        var ds = [];

        var columns = @(Html.RenderColumnsOption(typeof(MovieForm)));

        $(document).ready(function () {
            DatatableInit($('#datatable'), { columnDefs: columns, url: '/@areaname/@controllerName/GetList', useNumberCol:true})
        });
        function deleteListing() {
            if (!confirm("Apakah Anda yakin menghapus data ini ?")) {
                return false;
            }
        }

        function languageRender(data, type, row) {

            var icon = data === 'ID' ? 'indonesia.svg' : 'united-kingdom.svg';

            return `<div>
                    ${data}<img src="/assets/app/media/img/${icon}" height="15rem" style="margin-left:1rem;"/>
                </div>`
        }
    </script>

}
```

- Entry.cshtml
```html
@model App.Web.Areas.Samples.Model.MovieViewModel
@{
    ViewBag.Title = "Entry";
}

@Html.Partial("_ValidationMessage")
<div class="m-portlet">

    @using (Html.BeginForm("Entry", "Movie", new { area = "Samples" }, FormMethod.Post, new { id = "all", @class = "m-form" })) {
        <div class="m-portlet__body">
            <div class="m-form__section m-form__section--first" data-title="">
                @Html.AntiForgeryToken()
                <input type="hidden" name="@Html.NameFor(t => t.Form.Id)" value="@(Model.Form?.Id)" />

                @Html.FormFieldPartial((Model.Form?.Name ?? "").ToString(), new TextFieldSetting() {
                    InputName = Html.NameFor(t => t.Form.Name).ToString(),
                    Label = Html.LabelForRequired(e => e.Form.Name).ToString(),
                    HelpText = "",
                    Placeholder = "Name",
                    CssClass = "",
                    Style = TextFieldStyle.Simple
                })

                @Html.FormFieldPartial((Model.Form?.Category ?? "").ToString(), new SelectFieldSetting() {
                    InputName = Html.NameFor(t => t.Form.Category).ToString(),
                    Label = Html.LabelForRequired(e => e.Form.Category).ToString(),
                    HelpText = "",
                    Placeholder = "Category",
                    CssClass = "",
                    AddEmptyOption = true,
                    EmptyOptionLabel = "--Pilih--",
                    Options = Model.Categories,
                    OptionValueGetter = t => t.ToString(),
                    OptionLabelGetter = t => t.ToString()
                })

                @Html.FormFieldPartial((Model.Form?.Language ?? "").ToString(), new RadioFieldSetting() {
                   InputName = Html.NameFor(t => t.Form.Language).ToString(),
                   Label = Html.LabelForRequired(e => e.Form.Language).ToString(),
                   HelpText = "",
                   Placeholder = "Language",
                   CssClass = "",
                   Options = new List<string>() { "ID","EN" },
                   OptionValueGetter = t => t.DirectCastTo<string>(),
                   OptionLabelGetter = t => t.DirectCastTo<string>()
                })

                @Html.FormFieldPartial((Model.Form?.ReleaseDate), new DateTimeFieldSetting() {
                    InputName = Html.NameFor(t => t.Form.ReleaseDate).ToString(),
                    Label = Html.LabelForRequired(e => e.Form.ReleaseDate).ToString(),
                    HelpText = "",
                    Placeholder = "Name",
                    Style = DateTimeFieldStyle.DateOnly
                })

                @Html.FormFieldPartial(Model.Form?.Rating, new IntFieldSetting() {
                    InputName = Html.NameFor(t => t.Form.Rating).ToString(),
                    Label = Html.LabelForRequired(e => e.Form.Rating).ToString(),
                    HelpText = "",
                    Placeholder = "Rating",
                    CssClass = ""
                })

            </div>


        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <button type="submit" name="@Html.NameFor(t => t.Form.IsDraft).ToString()" value="true" class="btn btn-secondary">Save as Draft</button>
                <button type="submit" name="@Html.NameFor(t => t.Form.IsDraft).ToString()" value="false" class="btn btn-primary btnsave">Save</button>
            </div>
        </div>
    }

</div>
```

terlihat panjang, namun pada intinya view ini menggunakan [datatable helper](#64-datatable-helper) dan [Form field partial](#444-formfieldpartial) selain itu hanya kosmetik

untuk contoh pemakaian lebih lanjut dapat dilihat pada [sample project](#13-sample-project) pada menu ```Movies,Theatres dan ShowTimes```

### 7. Web.Config ###
Untuk memudahkan akses ```AppSetting``` pada ```Web.Config``` sebaiknya key appsetting didaftarkan pada ```App.Web\WebAppSettings```
```cs
public static class WebAppSettings {
    public static string SomeSettingKey => ConfigurationManager.AppSettings["Key"] ?? "";
    public static int SomeIntSetting => ConfigurationManager.AppSettings["Key2"].SafeConvert<int>();
}
```
dan digunakan dengan cara
```cs
void SomeMethod(){
    var setting = WebAppSettings.SomeSettingKey;
    var setting2 = WebAppSettings.SomeIntSetting;
}
```

### 8. Securities ###
#### 8.1. XSS Protection ####
Xss protextion dilakukan dengan cara meng-encode output dari sistem menjadi ```html encoded``` string. hal ini dilakukan tidak hanya dalam ```Razor Page``` atau ```.cshtml``` tetapi juga pada method ```Json``` yang biasanya di-consume oleh datatable dan ```selec2```

#### 8.2. IDOR Protection ####
Dilakukan dengan cara membatasi hak akses terhadap suatu page menggunakan role. 
pembatasan akses dilakukan baik dengan cara menghilangkan menu pada view dan juga dengan mem-protect action atau controller menggunakan attribute ```aplikasiAuth```

#### 8.3. Http Only Cookies ####
Menggunakan ```Http Only Cookies``` sehingga cookies tidak dapat diakses melalui javascript. hal ini dilakukan agar tidak terjadi *cookie stealing attack*

#### 8.4. Custom Error Handling ####
Dengan custom error handling. detail error tidak ditampilkan ke pada user sehingga mengurangi kemungkinan penyerangan terhadap aplikasi menggunakan informasi dari error information. 
untuk proses debugging pada server staging, custom error handling dapat dimatikan dengan cara me-nonaktifkan custom error handling pada ```web.config```
Ubah config ini
```xml
<customErrors mode="RemoteOnly" />
```
menjadi 
```xml
<customErrors mode="Off" />
```

### 9. Deployment ###
#### 9.1. Azure Deployment ####
Deployment ke server Azure dilakukan dengan menggunakan file publish profile yang di dapat dari ```Portal Azure```. lebih lengkap mengenai deployment ke azure dapat dilihat [di sini](https://docs.microsoft.com/en-us/visualstudio/deployment/tutorial-import-publish-settings-azure?view=vs-2019).
sebelum melakukan deploy pastikan versi aplikasi sudah diubah pada file ```App.Web\Utilities\CommonHelper.cs```. Ubah versi aplikasi pada method ```GetVersion()``` panduan melakukan perubahan nama versi dapat dilihat [di sini](#93-versioning-guide)

#### 9.3. Versioning Guide ####
Berikut ini adalah panduan melakukan update versi. contoh format versioning pada aplikasi adalah ```v1.4.13```. 
- angka ```1``` adalah fase pengembangan aplikasi. sampai dokumen ini dibuat fase aplikasi masih pada fase 1.
- angka ```4``` adalah increment sesuai jumlah deployment ke server Production yang di-reset apabila digit pertama berubah
- angka ```13``` adalah increment sesuai jumlah deployment ke server staging yang akan di-reset apabila digit ke 2 berubah

### 10. Contributing
langkah yang disarankan untuk melakukan develop framework adalah sebagai berikut 

1. Clone branch master
2. Buat branch baru dari master
3. Lakukan proses development dengan bantuan sample project
4. Update README.md apabila terdapat perubahan atau penambahan fungsi
5. Merge Request branch master
6. Buat branch baru dari branch master
7. lakukan pembersihan sample code dengan langkah berikut :

- Hapus Folder ```App.Web\Areas\Samples```
- Hapus File ```App.Web\Services\Repositories\SampleServices.cs```
- Hapus File ```App.Web\Services\Contracts\ISampleServices.cs```
- Hapus File ```App.DataAccess\Model\Sample.cs```
- Hapus Line didalam region Sample Model untuk mengapus sample table pada file ```App.DataAccess\ApplicationDbContext.cs```
```cs
#region Sample Model
public DbSet<Movie> Movies { get; set; }
public DbSet<Theatre> Theatres { get; set; }
public DbSet<ShowTime> ShowTimes { get; set; }
public DbSet<Ticket> Tickets { get; set; }
#endregion Sample Model
```
- Hapus Line didalam region Sampl ```App.Web\Services\ApplicationNinjectModule.cs```
```cs
//hapus line dalam region sample
#region Sample
Kernel.Bind<IMovieServices>().To<MovieServices>().InAppScope();
Kernel.Bind<ITheatreServices>().To<TheatreServices>().InAppScope();
Kernel.Bind<IShowTimeServices>().To<ShowTimeServices>().InAppScope();
Kernel.Bind<ITicketServices>().To<TicketServices>().InAppScope();
#endregion Sample
```
- Hapus Line didalam region Sample ```App.Web\App_Start\AutoMapperConfig.cs```
```cs
//hapus line dalam region sample
#region Sample
using App.Web.Areas.Samples.Model;
#endregion Sample
.
.
#region Sample
Kernel.Bind<IMovieServices>().To<MovieServices>().InAppScope();
Kernel.Bind<ITheatreServices>().To<TheatreServices>().InAppScope();
Kernel.Bind<IShowTimeServices>().To<ShowTimeServices>().InAppScope();
Kernel.Bind<ITicketServices>().To<TicketServices>().InAppScope();
#endregion Sample
```
- Hapus Line dalam region Sample pada ```App.Web\App_Start\Startup.Auth.cs```
```cs
#region Sample
var roleAction = context.ActionAuthorization.Where(t => t.ActionName == AppActions.User_Management_Roles).FirstOrDefault();

if (roleAction != null) {
    
    roleObj.Actions.Add(roleAction);

}
#endregion Sample
```
- Hapus line dalam region sample pada ```App.Web\Utilities\AppActions.cs```
- Hapus line dalam region sample pada ```App.Web\Utilities\AppModule.cs```

8. Merge ke branch ```bare-master```

